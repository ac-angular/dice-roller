import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  diceLeft: string = '';
  diceRight: string = '';
  numberOne: number = 1;
  numberTwo: number = 2;

  constructor() {
    this.updateDice();
  }

  throwDice(): void {
    this.numberOne = Math.round(Math.random() * 5) + 1;
    this.numberTwo = Math.round(Math.random() * 5) + 1;
    this.updateDice();
  }

  updateDice() {
    this.diceLeft = `./../assets/img/dice${this.numberOne}.png`;
    this.diceRight = `./../assets/img/dice${this.numberTwo}.png`;  
  }
}
